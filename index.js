const ws = require('ws');

const server = new ws.Server({ port: 3029 });

server.on('connection', (client) => {
  client.on('message', (message) => {
    console.log('received', message, JSON.parse(message));

    // retransmet la notification à tout le monde (dont le client Unity)
    server.clients.forEach(client => client.send(message));
  });
});
